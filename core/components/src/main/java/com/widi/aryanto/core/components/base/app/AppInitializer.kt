package com.widi.aryanto.core.components.base.app

interface AppInitializer {
    fun init(application: CoreApplication)
}