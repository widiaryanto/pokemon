package com.widi.aryanto.core.common.provider

interface NavigationProvider {
    fun openPokemonDetail(name: String, url: String)
    fun openAppLanguage()
    fun openAbout()
    fun navigateUp()
}