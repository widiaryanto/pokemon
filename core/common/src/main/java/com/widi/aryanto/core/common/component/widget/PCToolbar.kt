package com.widi.aryanto.core.common.component.widget

import androidx.annotation.StringRes
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.widi.aryanto.core.common.theme.PhinConColors
import com.widi.aryanto.core.common.theme.PhinConTypography
import com.widi.aryanto.core.common.theme.navigationBackIconColor

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PCToolbar(
    @StringRes titleResId: Int,
    scrollBehavior: TopAppBarScrollBehavior
) {
    var barState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (barState) 400.dp else 0.dp,
        tween(1000), label = ""
    )
    LaunchedEffect(Unit) {
        barState = false
    }
    CenterAlignedTopAppBar(
        scrollBehavior = scrollBehavior,
        title = {
            Text(
                stringResource(titleResId),
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth(),
                style = PhinConTypography.displayMedium
            )
        },
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = PhinConColors.primary
        ),
        modifier = Modifier
            .fillMaxWidth()
            .absoluteOffset(x = offsetAnimation)
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PCToolbarWithNavIcon(
    @StringRes titleResId: Int,
    pressOnBack: () -> Unit
) {
    var barState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (barState) 400.dp else 0.dp,
        tween(1000), label = ""
    )
    LaunchedEffect(Unit) {
        barState = false
    }
    TopAppBar(
        title = {
            Text(
                stringResource(titleResId),
                textAlign = TextAlign.Start,
                modifier = Modifier.fillMaxWidth(),
                style = PhinConTypography.displayMedium
            )
        },
        navigationIcon = {
            Icon(
                rememberVectorPainter(Icons.Filled.ArrowBack),
                contentDescription = null,
                tint = PhinConColors.navigationBackIconColor,
                modifier = Modifier
                    .padding(8.dp)
                    .clickable { pressOnBack.invoke() }
            )
        },
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = PhinConColors.primary
        ),
        modifier = Modifier
            .fillMaxWidth()
            .absoluteOffset(x = offsetAnimation)
    )
}