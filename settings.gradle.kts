pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "PhinCon"
include(":app")
include(":core:components")
include(":core:compose")
include(":core:common")
include(":features:splash")
include(":features:home")
include(":features:settings")
include(":features:pokemon")
