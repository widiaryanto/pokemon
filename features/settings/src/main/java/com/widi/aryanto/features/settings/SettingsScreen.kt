package com.widi.aryanto.features.settings

import android.content.res.Configuration
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import com.widi.aryanto.features.settings.view.SettingsContent
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.core.common.component.widget.PCToolbar
import com.widi.aryanto.core.common.provider.NavigationProvider
import com.widi.aryanto.core.common.R
import com.widi.aryanto.core.common.theme.PhinConTheme

@Destination(start = true)
@Composable
fun SettingsScreen(
    modifier: Modifier = Modifier,
    viewModel: SettingsViewModel = hiltViewModel(),
    navigator: NavigationProvider
) {
    val checkedState = remember { mutableStateOf(viewModel.isNightMode()) }

    SettingsBody(modifier) {
        SettingsContent(it, viewModel, checkedState, navigator)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun SettingsBody(
    modifier: Modifier = Modifier,
    pageContent: @Composable (PaddingValues) -> Unit
) {
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())
    Scaffold(
        modifier = modifier,
        topBar = { PCToolbar(R.string.toolbar_settings_title, scrollBehavior) },
        content = { pageContent.invoke(it) }
    )
}

@Preview(showBackground = true, name = "Light Mode")
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES, name = "Dark Mode")
@Composable
fun SettingsScreenPreview() {
    PhinConTheme {
        Surface {
            SettingsBody {
            }
        }
    }
}