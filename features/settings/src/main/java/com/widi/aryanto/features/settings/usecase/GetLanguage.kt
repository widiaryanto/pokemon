package com.widi.aryanto.features.settings.usecase

import com.widi.aryanto.core.components.usecase.ReturnUseCase
import com.widi.aryanto.features.settings.data.repository.LanguageRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetLanguage @Inject constructor(
    private val repository: LanguageRepository
) : ReturnUseCase<Unit, String>() {

    override suspend fun execute(params: Unit): Flow<String> {
        return repository.getLanguage
    }
}