package com.widi.aryanto.features.settings.model.dto

data class LanguageDto(
    val id: Int,
    val code: String,
    val name: String,
    var isSelected: Boolean = false
)