package com.widi.aryanto.features.settings.language

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.core.common.component.widget.PCToolbarWithNavIcon
import com.widi.aryanto.core.common.provider.NavigationProvider
import com.widi.aryanto.core.compose.SetLanguage
import com.widi.aryanto.core.common.R
import com.widi.aryanto.core.common.theme.PhinConTypography
import com.widi.aryanto.core.common.theme.Red700
import com.widi.aryanto.core.compose.clickableSingle
import kotlinx.coroutines.launch

@Destination
@Composable
fun LanguageScreen(
    viewModel: LanguageViewModel = hiltViewModel(),
    navigator: NavigationProvider
) {
    var langs by remember { viewModel.langs }
    val scope = rememberCoroutineScope()

    SetLanguage(langs.find { it.isSelected }?.code.toString())

    Scaffold(
        topBar = {
            PCToolbarWithNavIcon(
                R.string.toolbar_app_language_title,
                pressOnBack = {
                    navigator.navigateUp()
                }
            )
        },
        content = {
            LazyColumn(
                modifier = Modifier
                    .padding(it)
                    .fillMaxSize()
            ) {
                items(items = langs) { lang ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(50.dp)
                            .clickableSingle {
                                scope.launch {
                                    viewModel.saveLanguageCode(lang.code)
                                }
                                langs = langs.map { dto ->
                                    if (lang.id == dto.id) {
                                        dto.copy(isSelected = true)
                                    } else {
                                        dto.copy(isSelected = false)
                                    }
                                }
                            }
                            .padding(16.dp),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(text = lang.name, style = PhinConTypography.titleMedium)
                        AnimatedVisibility(lang.isSelected) {
                            Icon(
                                imageVector = Icons.Default.Check,
                                contentDescription = "Selected",
                                tint = Red700,
                                modifier = Modifier.size(20.dp)
                            )
                        }
                    }
                }
            }
        }
    )
}