package com.widi.aryanto.features.settings.di

import com.widi.aryanto.features.settings.data.repository.LanguageRepository
import com.widi.aryanto.features.settings.usecase.GetLanguage
import com.widi.aryanto.features.settings.usecase.SaveLanguage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LanguageModule {

    @Singleton
    @Provides
    fun provideSaveLanguage(repository: LanguageRepository): SaveLanguage {
        return SaveLanguage(repository)
    }

    @Singleton
    @Provides
    fun provideGetLanguage(repository: LanguageRepository): GetLanguage {
        return GetLanguage(repository)
    }
}