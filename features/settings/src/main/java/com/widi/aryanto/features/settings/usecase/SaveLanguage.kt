package com.widi.aryanto.features.settings.usecase

import com.widi.aryanto.core.components.usecase.LocalUseCase
import com.widi.aryanto.features.settings.data.repository.LanguageRepository
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class SaveLanguage @Inject constructor(
    private val repository: LanguageRepository
) : LocalUseCase<SaveLanguage.Params, Unit>() {
    data class Params(
        val language: String
    )

    override suspend fun FlowCollector<Unit>.execute(params: Params) {
        repository.setLanguage(params.language)
        emit(Unit)
    }
}