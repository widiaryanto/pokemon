package com.widi.aryanto.features.settings.about

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.core.common.R
import com.widi.aryanto.core.common.component.ExtraSmallSpacer
import com.widi.aryanto.core.common.component.MediumSpacer
import com.widi.aryanto.core.common.component.SmallSpacer
import com.widi.aryanto.core.common.component.widget.PCToolbarWithNavIcon
import com.widi.aryanto.core.common.provider.NavigationProvider
import com.widi.aryanto.core.common.theme.PhinConColors
import com.widi.aryanto.core.common.theme.PhinConShapes
import com.widi.aryanto.core.common.theme.PhinConTheme
import com.widi.aryanto.core.common.theme.PhinConTypography

@Destination
@Composable
fun AboutScreen(
    navigator: NavigationProvider
) {
    val uriHandler = LocalUriHandler.current
    val linkedinLink = "https://linkedin.com/in/widiaryanto"
    val gitlabLink = "https://gitlab.com/widiaryanto"

    Scaffold(
        topBar = {
            PCToolbarWithNavIcon(
                R.string.toolbar_about_title,
                pressOnBack = {
                    navigator.navigateUp()
                }
            )
        },
        content = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(it),
                contentAlignment = Alignment.Center
            ) {
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp),
                    shape = PhinConShapes.medium,
                    elevation = CardDefaults.cardElevation(
                        defaultElevation = 8.dp
                    )
                ) {
                    Column(
                        modifier = Modifier
                            .wrapContentHeight()
                            .padding(12.dp),
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_profile),
                            contentDescription = null,
                            modifier = Modifier
                                .size(140.dp)
                                .clip(CircleShape)
                        )
                        MediumSpacer()
                        Text(
                            text = "Muhamad Widi Aryanto",
                            style = PhinConTypography.displaySmall,
                            textAlign = TextAlign.Center
                        )
                        ExtraSmallSpacer()
                        Text(
                            text = "Android Developer",
                            style = PhinConTypography.headlineMedium,
                            textAlign = TextAlign.Center
                        )
                        ExtraSmallSpacer()
                        Text(
                            text = "Developed with concept of Modular Framework, Clean Architecture, and Jetpack Compose",
                            style = PhinConTypography.headlineMedium,
                            textAlign = TextAlign.Center
                        )
                        SmallSpacer()
                        ClickableText(
                            text = AnnotatedString(text = gitlabLink),
                            style = PhinConTypography.titleLarge,
                            onClick = {
                                uriHandler.openUri(gitlabLink)
                            }
                        )
                        SmallSpacer()
                        ClickableText(
                            text = AnnotatedString(text = linkedinLink),
                            style = PhinConTypography.titleLarge,
                            onClick = {
                                uriHandler.openUri(linkedinLink)
                            }
                        )
                    }
                }
            }
        }
    )
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Composable
fun AboutScreenPreview() {
    PhinConTheme {
        Surface(modifier = Modifier.background(PhinConColors.background)) {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                shape = PhinConShapes.medium,
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 8.dp
                )
            ) {
                Column(
                    modifier = Modifier
                        .wrapContentHeight()
                        .padding(12.dp),
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_profile),
                        contentDescription = null,
                        modifier = Modifier
                            .size(140.dp)
                            .clip(CircleShape)
                    )
                    MediumSpacer()
                    Text(
                        text = "Muhamad Widi Aryanto",
                        style = PhinConTypography.displaySmall,
                        textAlign = TextAlign.Center
                    )
                    ExtraSmallSpacer()
                    Text(
                        text = "Android Developer",
                        style = PhinConTypography.headlineMedium,
                        textAlign = TextAlign.Center
                    )
                    ExtraSmallSpacer()
                    Text(
                        text = "Developed with concept of Modular Framework, Clean Architecture, and Jetpack Compose",
                        style = PhinConTypography.headlineMedium,
                        textAlign = TextAlign.Center
                    )
                    SmallSpacer()
                    ClickableText(
                        text = AnnotatedString(text = "https://gitlab.com/widiaryanto"),
                        style = PhinConTypography.titleLarge,
                        onClick = {
                        }
                    )
                    SmallSpacer()
                    ClickableText(
                        text = AnnotatedString(text = "https://linkedin.com/in/widiaryanto"),
                        style = PhinConTypography.titleLarge,
                        onClick = {
                        }
                    )
                }
            }
        }
    }
}