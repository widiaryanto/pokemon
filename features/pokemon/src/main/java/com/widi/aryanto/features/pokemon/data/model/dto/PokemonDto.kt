package com.widi.aryanto.features.pokemon.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PokemonDto(
    val name: String,
    val url: String? = null,
    var isFavorite: Boolean = false
) : Parcelable {
    companion object {
        fun init() = PokemonDto(
            name = "ivysaur",
            url = "https://pokeapi.co/api/v2/pokemon/2/"
        )
    }
}