package com.widi.aryanto.features.pokemon.usecase.favorite

import com.widi.aryanto.core.components.usecase.LocalUseCase
import com.widi.aryanto.features.pokemon.data.repository.PokemonRepository
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class DeletePokemonFavorite @Inject constructor(
    private val repository: PokemonRepository
) : LocalUseCase<DeletePokemonFavorite.Params, Unit>() {

    data class Params(
        val name: String
    )

    override suspend fun FlowCollector<Unit>.execute(params: Params) {
        repository.deleteFavoriteByName(params.name)
        emit(Unit)
    }
}