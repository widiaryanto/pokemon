package com.widi.aryanto.features.pokemon.data.local.converter

import androidx.room.TypeConverter
import com.widi.aryanto.core.components.extension.fromJson
import com.widi.aryanto.core.components.extension.toJson

class StringListConverter {
    @TypeConverter
    fun toListOfStrings(stringValue: String): List<String>? {
        return stringValue.fromJson()
    }

    @TypeConverter
    fun fromListOfStrings(listOfString: List<String>?): String {
        return listOfString.toJson()
    }
}