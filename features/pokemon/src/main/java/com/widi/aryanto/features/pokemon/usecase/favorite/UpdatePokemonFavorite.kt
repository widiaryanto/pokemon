package com.widi.aryanto.features.pokemon.usecase.favorite

import com.widi.aryanto.core.components.usecase.LocalUseCase
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.data.model.dto.toPokemonEntity
import com.widi.aryanto.features.pokemon.data.repository.PokemonRepository
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class UpdatePokemonFavorite @Inject constructor(
    private val repository: PokemonRepository
) : LocalUseCase<UpdatePokemonFavorite.Params, Unit>() {

    data class Params(
        val pokemon: PokemonDto
    )

    override suspend fun FlowCollector<Unit>.execute(params: Params) {
        val dto = params.pokemon
        val character = repository.getFavorite(dto.name)
        if (character == null) {
            repository.saveFavorite(dto.toPokemonEntity())
        } else {
            repository.deleteFavoriteByName(dto.name)
        }
        emit(Unit)
    }
}