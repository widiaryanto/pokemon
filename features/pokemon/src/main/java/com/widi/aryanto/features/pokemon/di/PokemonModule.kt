package com.widi.aryanto.features.pokemon.di

import com.widi.aryanto.features.pokemon.usecase.favorite.AddPokemonFavorite
import com.widi.aryanto.features.pokemon.usecase.favorite.DeletePokemonFavorite
import com.widi.aryanto.features.pokemon.usecase.favorite.GetPokemonFavorites
import com.widi.aryanto.features.pokemon.usecase.favorite.UpdatePokemonFavorite
import com.widi.aryanto.features.pokemon.data.repository.PokemonRepository
import com.widi.aryanto.features.pokemon.usecase.GetPokemon
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class PokemonModule {

    @Singleton
    @Provides
    fun provideGetPokemon(repository: PokemonRepository): GetPokemon {
        return GetPokemon(repository)
    }

    @Singleton
    @Provides
    fun provideAddPokemonFavorite(repository: PokemonRepository): AddPokemonFavorite {
        return AddPokemonFavorite(repository)
    }

    @Singleton
    @Provides
    fun provideDeletePokemonFavorite(repository: PokemonRepository): DeletePokemonFavorite {
        return DeletePokemonFavorite(repository)
    }

    @Singleton
    @Provides
    fun provideGetPokemonFavorites(repository: PokemonRepository): GetPokemonFavorites {
        return GetPokemonFavorites(repository)
    }

    @Singleton
    @Provides
    fun provideUpdatePokemonFavorite(repository: PokemonRepository): UpdatePokemonFavorite {
        return UpdatePokemonFavorite(repository)
    }
}