package com.widi.aryanto.features.pokemon.data.model.dto

import com.widi.aryanto.features.pokemon.data.model.PokemonEntity
import com.widi.aryanto.features.pokemon.data.remote.PokemonInfo

fun PokemonInfo.toPokemonDto() = PokemonDto(
    name = name.orEmpty(),
    url = url
)

fun List<PokemonInfo>.toPokemonDtoList() = map { it.toPokemonDto() }

fun PokemonEntity.toPokemonDto() = PokemonDto(
    name = name,
    url = url
)

fun List<PokemonEntity>.toFavoriteDtoList() = map { it.toPokemonDto() }

fun PokemonDto.toPokemonEntity() = PokemonEntity(
    id = null,
    name = name,
    url = url.orEmpty()
)