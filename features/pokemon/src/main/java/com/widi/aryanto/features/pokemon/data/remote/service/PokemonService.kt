package com.widi.aryanto.features.pokemon.data.remote.service

import com.widi.aryanto.features.pokemon.data.remote.PokemonResponse
import retrofit2.http.*

interface PokemonService {
    @GET(POKEMON)
    suspend fun getPokemonList(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): PokemonResponse

    companion object {
        const val POKEMON = "/api/v2/pokemon"
    }
}