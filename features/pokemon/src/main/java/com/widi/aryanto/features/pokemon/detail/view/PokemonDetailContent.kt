package com.widi.aryanto.features.pokemon.detail.view

import android.content.res.Configuration
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import com.widi.aryanto.core.common.R
import com.widi.aryanto.core.common.component.DefaultSpacer
import com.widi.aryanto.core.common.component.widget.PCDivider
import com.widi.aryanto.core.common.theme.PhinConColors
import com.widi.aryanto.core.common.theme.PhinConTheme
import com.widi.aryanto.core.common.theme.PhinConTypography
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.list.PokemonEvent
import com.widi.aryanto.features.pokemon.list.PokemonViewModel
import kotlinx.coroutines.delay

@Composable
fun PokemonDetailContent(
    viewModel: PokemonViewModel = hiltViewModel(),
    paddingValues: PaddingValues,
    data: PokemonDto
) {
    var cardState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (cardState) 400.dp else 0.dp,
        tween(1000), label = ""
    )
    var name by remember { mutableStateOf("") }
    var isDialogVisible by remember { mutableStateOf(false) }
    var isFavorite by rememberSaveable(data) { mutableStateOf(data.isFavorite) }

    LaunchedEffect(Unit) {
        delay(200)
        cardState = false
    }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(paddingValues)
            .absoluteOffset(x = offsetAnimation),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.text_information),
            modifier = Modifier
                .padding(12.dp),
            style = PhinConTypography.titleLarge
        )
        Box(
            modifier = Modifier
                .fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(12.dp)
            ) {
                TextRow(
                    key = stringResource(id = R.string.text_name),
                    value = data.name
                )
                PCDivider()
                DefaultSpacer()
                TextRow(
                    key = stringResource(id = R.string.text_url),
                    value = data.url.toString()
                )
                PCDivider()
                DefaultSpacer()
            }
        }
        if (!data.isFavorite) {
            Button(onClick = { isDialogVisible = true }) {
                Text( text = stringResource(id = R.string.text_catch))
            }
        }
        if (isDialogVisible) {
            AlertDialogCatch(
                title = {
                    Text(
                        text = stringResource(id = R.string.text_enter_name),
                        style = MaterialTheme.typography.titleLarge,
                    )
                },
                content = {
                    OutlinedTextField(
                        value = name,
                        onValueChange = { name = it },
                        label = { Text(text = stringResource(id = R.string.text_name)) },
                    )
                },
                dismissButton = {
                    TextButton(
                        onClick = { isDialogVisible = false },
                        content = { Text(text = stringResource(id = R.string.text_cancel)) },
                    )
                },
                confirmButton = {
                    TextButton(
                        onClick = {
                            isFavorite = !isFavorite
                            data.isFavorite = isFavorite
                            viewModel.onTriggerEvent(PokemonEvent.AddOrRemoveFavorite(
                                PokemonDto(
                                    name = name.ifEmpty { "Mighty Pikachu" },
                                url = data.url
                            )
                            ))
                            isDialogVisible = false
                        },
                        content = { Text(text = stringResource(id = R.string.text_ok)) },
                    )
                },
                onDismiss = {
                    isDialogVisible = false
                },
            )
        }
    }
}

@Composable
private fun TextRow(key: String, value: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 12.dp, bottom = 12.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = key,
            maxLines = 1,
            overflow = TextOverflow.Visible,
            style = PhinConTypography.bodyMedium,
            textAlign = TextAlign.Start
        )
        Text(
            text = value,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            style = PhinConTypography.titleMedium,
            textAlign = TextAlign.End
        )
    }
}

@Composable
fun AlertDialogCatch(
    title: @Composable () -> Unit,
    content: @Composable () -> Unit,
    dismissButton: @Composable () -> Unit,
    confirmButton: @Composable () -> Unit,
    onDismiss: () -> Unit,
) {
    Dialog(onDismiss) {
        Surface(shape = MaterialTheme.shapes.medium) {
            Column {
                Column(Modifier.padding(24.dp)) {
                    title.invoke()
                    Spacer(Modifier.size(16.dp))
                    content.invoke()
                }
                Spacer(Modifier.size(4.dp))
                Row(
                    Modifier
                        .padding(8.dp)
                        .fillMaxWidth(),
                    Arrangement.spacedBy(8.dp, Alignment.End),
                ) {
                    dismissButton.invoke()
                    confirmButton.invoke()
                }
            }
        }
    }
}

@Preview(showBackground = true, name = "Light Mode")
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES, name = "Dark Mode")
@Composable
fun PokemonDetailScreenPreview() {
    PhinConTheme {
        Surface(color = PhinConColors.background) {
        }
    }
}