package com.widi.aryanto.features.pokemon.list.view

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto

@Composable
fun PokemonFavoriteContent(
    modifier: Modifier,
    favors: List<PokemonDto>,
    selectedFavorite: MutableState<PokemonDto>,
    onDetailItem: (PokemonDto) -> Unit = {},
    onDeleteItem: (String) -> Unit = {}
) {
    LazyColumn(
        modifier = modifier
    ) {
        itemsIndexed(favors, key = { _, item -> item.name}) { _, favor ->
            PokemonFavoriteRow(
                dto = favor,
                onDetailClick = {
                    onDetailItem.invoke(favor)
                },
                onDeleteClick = {
                    selectedFavorite.value = favor
                    onDeleteItem.invoke(favor.name)
                }
            )
        }
    }
}