package com.widi.aryanto.features.pokemon.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.widi.aryanto.features.pokemon.data.local.converter.StringListConverter
import com.widi.aryanto.features.pokemon.data.local.dao.PokemonFavoriteDao
import com.widi.aryanto.features.pokemon.data.model.PokemonEntity

@Database(
    entities = [PokemonEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(StringListConverter::class)
abstract class PhinConDatabase : RoomDatabase() {
    abstract fun pokemonFavoriteDao(): PokemonFavoriteDao
}