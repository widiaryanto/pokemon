package com.widi.aryanto.features.pokemon.usecase.favorite

import com.widi.aryanto.core.components.usecase.LocalUseCase
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.data.model.dto.toPokemonEntity
import com.widi.aryanto.features.pokemon.data.repository.PokemonRepository
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class AddPokemonFavorite @Inject constructor(
    private val repository: PokemonRepository
) : LocalUseCase<AddPokemonFavorite.Params, Unit>() {

    data class Params(
        val pokemon: PokemonDto
    )

    override suspend fun FlowCollector<Unit>.execute(params: Params) {
        val dto = params.pokemon
        repository.saveFavorite(dto.toPokemonEntity())
        emit(Unit)
    }
}