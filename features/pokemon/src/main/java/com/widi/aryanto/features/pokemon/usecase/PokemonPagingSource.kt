package com.widi.aryanto.features.pokemon.usecase

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.data.model.dto.toPokemonDtoList
import com.widi.aryanto.features.pokemon.data.repository.PokemonRepository
import java.io.IOException

class PokemonPagingSource(
    private val repository: PokemonRepository,
    private val options: Int
) : PagingSource<Int, PokemonDto>() {
    override fun getRefreshKey(state: PagingState<Int, PokemonDto>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PokemonDto> {
        val page = params.key ?: 1
        return try {
            val response = repository.getPokemonList(page, options)
            val pokemonList = response.results.orEmpty().toPokemonDtoList()

            pokemonList.map {
                val pokemonFav = repository.getFavorite(it.name)
                it.isFavorite = pokemonFav != null
            }

            LoadResult.Page(
                data = pokemonList,
                prevKey = if (page == 1) null else page - 1,
                nextKey = if (pokemonList.isEmpty()) null else page + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        }
    }
}