package com.widi.aryanto.features.pokemon.list.view

import android.content.res.Configuration
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.widi.aryanto.core.common.theme.PhinConColors
import com.widi.aryanto.core.common.theme.PhinConTheme
import com.widi.aryanto.core.common.theme.PhinConTypography
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PokemonRow(
    dto: PokemonDto,
    onDetailClick: () -> Unit = {}
) {
    Card(
        onClick = onDetailClick,
        modifier = Modifier
            .fillMaxHeight()
            .padding(
                vertical = 4.dp,
                horizontal = 8.dp
            ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 8.dp
        )
    ) {
        Column(
            modifier = Modifier.fillMaxHeight().padding(16.dp)
        ) {
            Text(
                text = dto.name,
                modifier = Modifier.fillMaxWidth(),
                style = PhinConTypography.titleLarge
            )
        }
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun PokemonRowPreview() {
    PhinConTheme {
        Surface(color = PhinConColors.background) {
            PokemonRow(dto = PokemonDto.init())
        }
    }
}