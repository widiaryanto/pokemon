package com.widi.aryanto.features.pokemon.usecase

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.widi.aryanto.core.components.usecase.FlowPagingUseCase
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.data.repository.PokemonRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPokemon @Inject constructor(
    private val repository: PokemonRepository
) : FlowPagingUseCase<GetPokemon.Params, PokemonDto>() {

    data class Params(
        val pagingConfig: PagingConfig,
        val options: Int
    )

    override fun execute(params: Params): Flow<PagingData<PokemonDto>> {
        return Pager(
            config = params.pagingConfig,
            pagingSourceFactory = { PokemonPagingSource(repository, params.options) }
        ).flow
    }
}