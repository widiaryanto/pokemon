package com.widi.aryanto.features.pokemon.list

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.widi.aryanto.core.components.base.mvi.BaseViewState
import com.widi.aryanto.core.components.base.mvi.MviViewModel
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.usecase.GetPokemon
import com.widi.aryanto.features.pokemon.usecase.favorite.DeletePokemonFavorite
import com.widi.aryanto.features.pokemon.usecase.favorite.GetPokemonFavorites
import com.widi.aryanto.features.pokemon.usecase.favorite.UpdatePokemonFavorite
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PokemonViewModel @Inject constructor(
    private val getPokemon: GetPokemon,
    private val updatePokemonFavorite: UpdatePokemonFavorite,
    private val getFavorites: GetPokemonFavorites,
    private val deleteFavorite: DeletePokemonFavorite
) : MviViewModel<BaseViewState<PokemonState>, PokemonEvent>() {

    private val config = PagingConfig(pageSize = 10)

    override fun onTriggerEvent(eventType: PokemonEvent) {
        when (eventType) {
            is PokemonEvent.LoadPokemon -> onLoadPokemon()
            is PokemonEvent.AddOrRemoveFavorite -> onAddOrRemoveFavorite(eventType.pokemon)
            is PokemonEvent.DeleteFavorite -> onDeleteFavorite(eventType.name)
            is PokemonEvent.LoadFavorites -> onLoadFavorites()
        }
    }

    private fun onLoadPokemon() = safeLaunch {
        setState(BaseViewState.Loading)
        val params = GetPokemon.Params(config, 1)
        val pagedFlow = getPokemon(params).cachedIn(scope = viewModelScope)
        setState(BaseViewState.Data(PokemonState(pagedData = pagedFlow)))
    }

    private fun onAddOrRemoveFavorite(dto: PokemonDto) = safeLaunch {
        val params = UpdatePokemonFavorite.Params(dto)
        call(updatePokemonFavorite(params))
    }

    private fun onLoadFavorites() = safeLaunch {
        call(getFavorites(Unit)) {
            if (it.isEmpty()) {
                setState(BaseViewState.Empty)
            } else {
                setState(BaseViewState.Data(PokemonState(favorList = it)))
            }
        }
    }

    private fun onDeleteFavorite(name: String) = safeLaunch {
        call(deleteFavorite(DeletePokemonFavorite.Params(name))) {
            onTriggerEvent(PokemonEvent.LoadFavorites)
        }
    }
}