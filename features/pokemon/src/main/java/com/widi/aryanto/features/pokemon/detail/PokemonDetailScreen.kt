package com.widi.aryanto.features.pokemon.detail

import android.content.res.Configuration
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.core.common.component.widget.PCToolbarWithNavIcon
import com.widi.aryanto.core.common.provider.NavigationProvider
import com.widi.aryanto.core.common.R
import com.widi.aryanto.core.common.theme.PhinConColors
import com.widi.aryanto.core.common.theme.PhinConTheme
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.detail.view.PokemonDetailContent

@Destination(start = true)
@Composable
fun PokemonDetailScreen(
    name: String = "",
    url: String? = "",
    navigator: NavigationProvider
) {
    PokemonDetailBody(
        pressOnBack = { navigator.navigateUp() }
    ) {
        PokemonDetailContent(
            paddingValues = it,
            data = PokemonDto(
                name = name,
                url = url
            )
        )
    }
}

@Composable
private fun PokemonDetailBody(
    pressOnBack: () -> Unit = {},
    pageContent: @Composable (PaddingValues) -> Unit
) {
    Scaffold(
        topBar = {
            PCToolbarWithNavIcon(
                R.string.toolbar_pokemon_detail_title,
                pressOnBack = pressOnBack
            )
        },
        content = { pageContent.invoke(it) }
    )
}

@Preview(showBackground = true, name = "Light Mode")
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES, name = "Dark Mode")
@Composable
fun PokemonDetailScreenPreview() {
    PhinConTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = PhinConColors.background
        ) {
            PokemonDetailBody { }
        }
    }
}