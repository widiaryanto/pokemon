package com.widi.aryanto.features.pokemon.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.widi.aryanto.core.components.room.dao.BaseDao
import com.widi.aryanto.features.pokemon.data.model.PokemonEntity

@Dao
interface PokemonFavoriteDao : BaseDao<PokemonEntity> {
    @Query("SELECT * FROM ${PokemonEntity.TABLE_NAME}")
    suspend fun getFavoriteList(): List<PokemonEntity>

    @Query("SELECT * FROM ${PokemonEntity.TABLE_NAME} WHERE name = :favoriteName")
    suspend fun getFavorite(favoriteName: String): PokemonEntity?

    @Query("DELETE FROM ${PokemonEntity.TABLE_NAME}")
    suspend fun deleteFavoriteList()

    @Query("DELETE FROM ${PokemonEntity.TABLE_NAME} WHERE name = :favoriteName")
    suspend fun deleteFavoriteByName(favoriteName: String)
}