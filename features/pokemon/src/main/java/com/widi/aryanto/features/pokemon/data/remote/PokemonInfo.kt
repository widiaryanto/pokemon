package com.widi.aryanto.features.pokemon.data.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PokemonInfo(
    @Json(name = "name") val name: String?,
    @Json(name = "url") val url: String?
)