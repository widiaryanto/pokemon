package com.widi.aryanto.features.pokemon.list.view

import android.content.res.Configuration
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.widi.aryanto.core.common.theme.PhinConColors
import com.widi.aryanto.core.common.theme.PhinConTheme
import com.widi.aryanto.core.common.theme.PhinConTypography
import com.widi.aryanto.core.common.theme.RedDark
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PokemonFavoriteRow(
    dto: PokemonDto,
    onDetailClick: () -> Unit = {},
    onDeleteClick: () -> Unit = {}
) {
    Card(
        onClick = onDetailClick,
        modifier = Modifier
            .fillMaxHeight()
            .padding(
                vertical = 4.dp,
                horizontal = 8.dp
            ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 8.dp
        )
    ) {
        Row(
            modifier = Modifier.fillMaxWidth().padding(16.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {

            Text(
                modifier = Modifier.weight(1F),
                text = dto.name,
                style = PhinConTypography.titleLarge
            )

            IconButton(
                onClick = onDeleteClick,
                modifier = Modifier
                    .width(34.dp)
                    .height(34.dp)
            ) {
                Icon(
                    painter = rememberVectorPainter(Icons.Default.Delete),
                    contentDescription = null,
                    tint = RedDark
                )
            }
        }
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun EpisodeFavoriteRowPreview() {
    PhinConTheme {
        Surface(color = PhinConColors.background) {
            PokemonFavoriteRow(dto = PokemonDto.init())
        }
    }
}