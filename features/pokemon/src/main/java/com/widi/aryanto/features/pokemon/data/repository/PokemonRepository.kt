package com.widi.aryanto.features.pokemon.data.repository

import com.widi.aryanto.features.pokemon.data.local.dao.PokemonFavoriteDao
import com.widi.aryanto.features.pokemon.data.model.PokemonEntity
import com.widi.aryanto.features.pokemon.data.remote.service.PokemonService
import javax.inject.Inject

class PokemonRepository
@Inject
constructor(
    private val service: PokemonService,
    private val dao: PokemonFavoriteDao
) {
    suspend fun getPokemonList(
        page: Int,
        offset: Int
    ) = service.getPokemonList(page, offset)

    suspend fun getFavoriteList() = dao.getFavoriteList()

    suspend fun getFavorite(favoriteName: String) = dao.getFavorite(favoriteName)

    suspend fun deleteFavoriteByName(favoriteName: String) = dao.deleteFavoriteByName(favoriteName)

    suspend fun saveFavorite(entity: PokemonEntity) = dao.insert(entity)

    suspend fun saveFavoriteList(entityList: List<PokemonEntity>) = dao.insert(entityList)
}