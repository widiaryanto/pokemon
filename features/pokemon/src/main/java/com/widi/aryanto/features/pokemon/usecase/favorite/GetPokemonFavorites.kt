package com.widi.aryanto.features.pokemon.usecase.favorite

import com.widi.aryanto.core.components.usecase.LocalUseCase
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.data.model.dto.toFavoriteDtoList
import com.widi.aryanto.features.pokemon.data.repository.PokemonRepository
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class GetPokemonFavorites @Inject constructor(
    private val repository: PokemonRepository
) : LocalUseCase<Unit, List<PokemonDto>>() {

    override suspend fun FlowCollector<List<PokemonDto>>.execute(params: Unit) {
        val favors = repository.getFavoriteList()
        emit(favors.toFavoriteDtoList())
    }
}