package com.widi.aryanto.features.pokemon.data.remote

import com.widi.aryanto.features.pokemon.data.remote.base.PageInfo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PokemonResponse(
    @Json(name = "info") val pageInfo: PageInfo?,
    @Json(name = "results") val results: List<PokemonInfo>?
)