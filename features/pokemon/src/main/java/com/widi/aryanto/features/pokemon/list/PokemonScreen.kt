package com.widi.aryanto.features.pokemon.list

import android.content.res.Configuration
import androidx.annotation.StringRes
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetState
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.TabRowDefaults
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.material.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material.TabRowDefaults.tabIndicatorOffset
import androidx.compose.material.Text
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.widi.aryanto.core.common.provider.NavigationProvider
import com.widi.aryanto.core.common.theme.PhinConColors
import com.widi.aryanto.core.common.theme.PhinConTypography
import com.widi.aryanto.core.common.theme.RedDark
import com.widi.aryanto.core.components.base.mvi.BaseViewState
import com.widi.aryanto.core.common.R
import com.widi.aryanto.core.common.component.widget.EmptyView
import com.widi.aryanto.core.common.component.widget.ErrorView
import com.widi.aryanto.core.common.component.widget.LoadingView
import com.widi.aryanto.core.common.component.widget.LottieEmptyView
import com.widi.aryanto.core.common.component.widget.LottieErrorView
import com.widi.aryanto.core.common.component.widget.PCToolbar
import com.widi.aryanto.core.common.theme.PhinConTheme
import com.widi.aryanto.core.components.extension.cast
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import com.widi.aryanto.features.pokemon.list.view.PokemonBottomSheetContent
import com.widi.aryanto.features.pokemon.list.view.PokemonContent
import com.widi.aryanto.features.pokemon.list.view.PokemonFavoriteContent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun PokemonScreen(
    modifier: Modifier = Modifier,
    viewModel: PokemonViewModel = hiltViewModel(),
    navigator: NavigationProvider
) {
    val uiState by viewModel.uiState.collectAsState()

    val bottomSheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val coroutineScope = rememberCoroutineScope()
    val selectedFavorite = remember { mutableStateOf(PokemonDto.init()) }

    PokemonBody(modifier = modifier, bottomSheetState = bottomSheetState, sheetContent = {
        PokemonBottomSheetContent(
            pokemon = selectedFavorite.value,
            onCancel = {
                coroutineScope.launch {
                    bottomSheetState.hide()
                }
            },
            onApprove = {
                coroutineScope.launch {
                    viewModel.onTriggerEvent(PokemonEvent.DeleteFavorite(selectedFavorite.value.name))
                    bottomSheetState.hide()
                }
            }
        )
    }) { padding ->
        PokemonTab(
            uiState,
            viewModel,
            padding,
            navigator,
            coroutineScope,
            bottomSheetState,
            selectedFavorite
        )
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun PokemonTab(
    uiState: BaseViewState<*>,
    viewModel: PokemonViewModel,
    padding: PaddingValues,
    navigator: NavigationProvider,
    coroutineScope: CoroutineScope,
    bottomSheetState: ModalBottomSheetState,
    selectedFavorite: MutableState<PokemonDto>
) {
    Column {
        val tabsName = remember { PokemonTabs.values().map { it.value } }
        val selectedIndex = rememberSaveable { mutableStateOf(PokemonTabs.POKEMON_LIST.ordinal) }
        var tabState by remember { mutableStateOf(true) }
        val offsetAnimation: Dp by animateDpAsState(
            if (tabState) 400.dp else 0.dp,
            tween(1000), label = ""
        )
        LaunchedEffect(Unit) {
            delay(200)
            tabState = false
        }
        TabRow(
            selectedTabIndex = selectedIndex.value,
            backgroundColor = PhinConColors.primary,
            modifier = Modifier.padding(padding).absoluteOffset(x = offsetAnimation),
            indicator = { tabPositions ->
                TabRowDefaults.Indicator(
                    color = RedDark,
                    height = TabRowDefaults.IndicatorHeight,
                    modifier = Modifier
                        .tabIndicatorOffset(tabPositions[selectedIndex.value])
                )
            }
        ) {
            tabsName.forEachIndexed { index, stringResourceId ->
                Tab(
                    selected = index == selectedIndex.value,
                    onClick = {
                        when (stringResourceId) {
                            PokemonTabs.POKEMON_LIST.value -> {
                                selectedIndex.value = PokemonTabs.POKEMON_LIST.ordinal
                            }
                            PokemonTabs.FAVORITE_LIST.value -> {
                                selectedIndex.value = PokemonTabs.FAVORITE_LIST.ordinal
                            }
                        }
                    },
                    text = {
                        Text(
                            text = stringResource(id = stringResourceId),
                            style = PhinConTypography.headlineMedium
                        )
                    }
                )
            }
        }
        when (selectedIndex.value) {
            PokemonTabs.POKEMON_LIST.ordinal -> {
                PokemonPage(uiState, viewModel, navigator)
            }
            PokemonTabs.FAVORITE_LIST.ordinal -> {
                FavoritesPage(
                    coroutineScope,
                    bottomSheetState,
                    uiState,
                    viewModel,
                    navigator,
                    selectedFavorite
                )
            }
        }
    }
}

private enum class PokemonTabs(@StringRes val value: Int) {
    POKEMON_LIST(R.string.text_pokemon_list),
    FAVORITE_LIST(R.string.text_favorite_list);
}

@Composable
private fun PokemonPage(
    uiState: BaseViewState<*>,
    viewModel: PokemonViewModel,
    navigator: NavigationProvider
) {
    var pokemonState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (pokemonState) 400.dp else 0.dp,
        tween(1000), label = ""
    )
    LaunchedEffect(Unit) {
        delay(400)
        pokemonState = false
    }
    when (uiState) {
        is BaseViewState.Data -> PokemonContent(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 4.dp)
                .absoluteOffset(x = offsetAnimation),
            viewState = uiState.cast<BaseViewState.Data<PokemonState>>().value,
            selectItem = { data -> navigator.openPokemonDetail(
                name = data.name,
                url = data.url.toString()
            ) }
        )
        is BaseViewState.Empty -> EmptyView(
            modifier = Modifier
                .fillMaxSize()
                .absoluteOffset(x = offsetAnimation)
        )
        is BaseViewState.Error -> ErrorView(
            modifier = Modifier.absoluteOffset(x = offsetAnimation),
            e = uiState.cast<BaseViewState.Error>().throwable,
            action = {
                viewModel.onTriggerEvent(PokemonEvent.LoadPokemon)
            }
        )
        is BaseViewState.Loading -> LoadingView(
            modifier = Modifier.absoluteOffset(x = offsetAnimation),
        )
    }

    LaunchedEffect(key1 = viewModel, block = {
        viewModel.onTriggerEvent(PokemonEvent.LoadPokemon)
    })
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun FavoritesPage(
    coroutineScope: CoroutineScope,
    bottomSheetState: ModalBottomSheetState,
    uiState: BaseViewState<*>,
    viewModel: PokemonViewModel,
    navigator: NavigationProvider,
    selectedFavorite: MutableState<PokemonDto>
) {
    var pokemonState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (pokemonState) 400.dp else 0.dp,
        tween(1000), label = ""
    )
    LaunchedEffect(Unit) {
        delay(400)
        pokemonState = false
    }
    when (uiState) {
        is BaseViewState.Data -> PokemonFavoriteContent(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 4.dp)
                .absoluteOffset(x = offsetAnimation),
            favors = uiState.cast<BaseViewState.Data<PokemonState>>().value.favorList,
            selectedFavorite = selectedFavorite,
            onDetailItem = { data -> navigator.openPokemonDetail(
                name = data.name,
                url = data.url.toString()
            ) },
            onDeleteItem = {
                coroutineScope.launch {
                    if (bottomSheetState.isVisible) {
                        bottomSheetState.hide()
                    } else {
                        bottomSheetState.show()
                    }
                }
            }
        )
        is BaseViewState.Empty -> LottieEmptyView(
            modifier = Modifier
                .fillMaxSize()
                .absoluteOffset(x = offsetAnimation)
        )
        is BaseViewState.Error -> LottieErrorView(
            modifier = Modifier.absoluteOffset(x = offsetAnimation),
            e = uiState.cast<BaseViewState.Error>().throwable,
            action = {
                viewModel.onTriggerEvent(PokemonEvent.LoadFavorites)
            }
        )
        is BaseViewState.Loading -> LoadingView(
            modifier = Modifier.absoluteOffset(x = offsetAnimation)
        )
    }

    LaunchedEffect(key1 = viewModel, block = {
        viewModel.onTriggerEvent(PokemonEvent.LoadFavorites)
    })
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class)
@Composable
private fun PokemonBody(
    modifier: Modifier = Modifier,
    bottomSheetState: ModalBottomSheetState,
    sheetContent: @Composable ColumnScope.() -> Unit,
    pageContent: @Composable (PaddingValues) -> Unit
) {
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())
    ModalBottomSheetLayout(
        sheetContent = sheetContent,
        modifier = modifier
            .fillMaxSize(),
        sheetState = bottomSheetState,
        sheetContentColor = PhinConColors.background,
        sheetShape = RectangleShape,
        content = {
            Scaffold(
                modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
                topBar = { PCToolbar(R.string.toolbar_pokemon_title, scrollBehavior) },
                content = { pageContent.invoke(it) }
            )
        }
    )
}

@Preview(showBackground = true, name = "Light Mode")
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES, name = "Dark Mode")
@Composable
fun PokemonScreenPreview() {
    PhinConTheme {
        Surface {
        }
    }
}