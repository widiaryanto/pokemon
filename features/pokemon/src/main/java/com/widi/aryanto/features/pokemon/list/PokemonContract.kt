package com.widi.aryanto.features.pokemon.list

import androidx.paging.PagingData
import com.widi.aryanto.features.pokemon.data.model.dto.PokemonDto
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow

data class PokemonState(
    val pagedData: Flow<PagingData<PokemonDto>> = emptyFlow(),
    val favorList: List<PokemonDto> = emptyList()
)

sealed class PokemonEvent {
    object LoadPokemon : PokemonEvent()
    data class AddOrRemoveFavorite(val pokemon: PokemonDto) : PokemonEvent()
    object LoadFavorites : PokemonEvent()
    data class DeleteFavorite(val name: String) : PokemonEvent()
}