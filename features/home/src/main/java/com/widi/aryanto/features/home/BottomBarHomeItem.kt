package com.widi.aryanto.features.home

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.graphics.vector.ImageVector
import com.widi.aryanto.core.common.R

enum class BottomBarHomeItem(
    @StringRes val title: Int,
    val icon: ImageVector
) {
    MAIN(
        title = R.string.bottom_menu_home,
        icon = Icons.Filled.Home
    ),
    MENU(
        title = R.string.bottom_menu_menu,
        icon = Icons.Filled.ArrowDropUp
    ),
    SETTINGS(
        title = R.string.bottom_menu_settings,
        icon = Icons.Filled.Settings
    );
}