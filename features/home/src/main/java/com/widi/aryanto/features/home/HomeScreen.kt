package com.widi.aryanto.features.home

import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Dashboard
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.widi.aryanto.core.common.provider.NavigationProvider
import com.widi.aryanto.core.common.theme.PhinConColors
import com.widi.aryanto.core.common.theme.RalewayFonts
import com.widi.aryanto.core.common.theme.selectedBottomItemColor
import com.widi.aryanto.core.common.theme.unselectedBottomItemColor
import com.google.accompanist.insets.navigationBarsPadding
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.features.pokemon.list.PokemonScreen
import com.widi.aryanto.features.settings.SettingsScreen
import kotlinx.coroutines.launch

@Destination(start = true)
@Composable
fun HomeScreen(navigator: NavigationProvider) {
    val (currentBottomTab, setCurrentBottomTab) = rememberSaveable {
        mutableStateOf(BottomBarHomeItem.MAIN)
    }

    val animatableSize = remember { Animatable(Size.Zero, Size.VectorConverter) }
    val (imageSize, setImageSize) = remember { mutableStateOf<Size?>(null) }
    val density = LocalDensity.current
    val scope = rememberCoroutineScope()
    LaunchedEffect(Unit) {
        scope.launch {
            animatableSize.animateTo(
                Size(width = 200F, height = 200F),
                animationSpec = tween(durationMillis = 1000)
            )
        }
    }
    Crossfade(currentBottomTab, label = "") { bottomTab ->
        Scaffold(
            bottomBar = { HomeBottomNavigation(bottomTab, setCurrentBottomTab) },
            floatingActionButton = {
                Box {
                    FloatingActionButton(
                        onClick = {
                            setCurrentBottomTab.invoke(BottomBarHomeItem.MENU)
                        },
                        shape = CircleShape,
                        modifier = Modifier
                            .then(
                                if (animatableSize.value != Size.Zero) {
                                    animatableSize.value.run {
                                        Modifier.size(
                                            width = with(density) { width.toDp() },
                                            height = with(density) { height.toDp() },
                                        )
                                    }
                                } else {
                                    Modifier
                                }
                            )
                            .align(Alignment.Center)
                            .offset(y = 50.dp)
                            .onSizeChanged {
                                if (imageSize != null) return@onSizeChanged
                                val size = Size(width = 1000.dp.value, height = 1000.dp.value)
                                setImageSize(size)
                                scope.launch {
                                    animatableSize.snapTo(size)
                                }
                            }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Dashboard,
                            contentDescription = null,
                            modifier = Modifier.size(45.dp)
                        )
                    }
                }
            },
            floatingActionButtonPosition = FabPosition.Center,
            content = {
                val modifier = Modifier.padding(it)
                when (bottomTab) {
                    BottomBarHomeItem.MAIN -> PokemonScreen(
                        modifier = modifier,
                        navigator = navigator
                    )
                    BottomBarHomeItem.MENU -> SettingsScreen(
                        modifier = modifier,
                        navigator = navigator
                    )
                    BottomBarHomeItem.SETTINGS -> SettingsScreen(
                        modifier = modifier,
                        navigator = navigator
                    )
                }
            }
        )
    }
}

@Composable
private fun HomeBottomNavigation(
    bottomTab: BottomBarHomeItem,
    setCurrentBottomTab: (BottomBarHomeItem) -> Unit
) {
    val pages = BottomBarHomeItem.values()

    NavigationBar(
        containerColor = PhinConColors.primary,
        modifier = Modifier.fillMaxWidth()
    ) {
        pages.forEach { page ->
            val selected = page == bottomTab
            val selectedLabelColor = if (selected) {
                selectedBottomItemColor
            } else {
                unselectedBottomItemColor
            }
            NavigationBarItem(
                icon = {
                    Icon(
                        painter = rememberVectorPainter(image = page.icon),
                        contentDescription = stringResource(page.title)
                    )
                },
                label = {
                    Text(
                        text = stringResource(page.title),
                        color = selectedLabelColor,
                        fontSize = 12.sp,
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = RalewayFonts
                    )
                },
                selected = selected,
                onClick = {
                    setCurrentBottomTab.invoke(page)
                },
                colors = NavigationBarItemDefaults.colors(
                    selectedIconColor = selectedBottomItemColor,
                    unselectedIconColor = unselectedBottomItemColor,
                    indicatorColor = PhinConColors.primary
                ),
                alwaysShowLabel = true,
                modifier = Modifier.navigationBarsPadding()
            )
        }
    }
}