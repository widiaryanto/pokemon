package com.widi.aryanto.phincon.di

import android.content.Context
import com.widi.aryanto.core.components.base.app.AppInitializer
import com.widi.aryanto.core.components.base.app.AppInitializerImpl
import com.widi.aryanto.phincon.app.PhinConApp
import com.widi.aryanto.core.components.base.app.NetworkConfig
import com.widi.aryanto.core.components.base.app.TimberInitializer
import com.widi.aryanto.core.components.pref.CacheManager
import com.widi.aryanto.phincon.app.NetworkConfigs
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    @Singleton
    fun provideApplication(): PhinConApp {
        return PhinConApp()
    }

    @Provides
    @Singleton
    fun provideNetworkConfig(): NetworkConfig {
        return NetworkConfigs()
    }

    @Provides
    @Singleton
    fun provideCacheManager(@ApplicationContext context: Context): CacheManager {
        return CacheManager(context)
    }

    @Provides
    @Singleton
    fun provideTimberInitializer(
        networkConfig: NetworkConfig
    ) = TimberInitializer()

    @Provides
    @Singleton
    fun provideAppInitializer(
        timberInitializer: TimberInitializer
    ): AppInitializer {
        return AppInitializerImpl(timberInitializer)
    }
}