package com.widi.aryanto.phincon.di

import android.content.Context
import androidx.room.Room
import com.widi.aryanto.features.pokemon.data.local.dao.PokemonFavoriteDao
import com.widi.aryanto.features.pokemon.data.local.db.PhinConDatabase
import com.widi.aryanto.phincon.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

private const val DB_NAME = "db_name"

@Module
@InstallIn(SingletonComponent::class)
class LocalModule {
    @Provides
    @Singleton
    @Named(value = DB_NAME)
    fun provideDatabaseName(): String {
        return BuildConfig.DB_NAME
    }

    @Provides
    @Singleton
    fun provideDatabase(
        @Named(value = DB_NAME) dbname: String,
        @ApplicationContext context: Context
    ): PhinConDatabase {
        return Room.databaseBuilder(context, PhinConDatabase::class.java, dbname)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun providePokemonFavoriteDao(appDatabase: PhinConDatabase): PokemonFavoriteDao {
        return appDatabase.pokemonFavoriteDao()
    }
}