package com.widi.aryanto.phincon.di

import android.content.Context
import com.widi.aryanto.core.common.provider.LanguageProvider
import com.widi.aryanto.core.common.provider.ResourceProvider
import com.widi.aryanto.core.common.provider.ThemeProvider
import com.widi.aryanto.core.components.pref.CacheManager
import com.widi.aryanto.phincon.providers.AppLanguageProvider
import com.widi.aryanto.phincon.providers.AppResourceProvider
import com.widi.aryanto.phincon.providers.AppThemeProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ProviderModule {

    @Provides
    @Singleton
    fun provideThemeProvider(@ApplicationContext context: Context): ThemeProvider {
        return AppThemeProvider(context)
    }

    @Provides
    @Singleton
    fun provideAppResourceProvider(@ApplicationContext context: Context): ResourceProvider {
        return AppResourceProvider(context)
    }

    @Provides
    @Singleton
    fun provideAppLanguageProvider(cacheManager: CacheManager): LanguageProvider {
        return AppLanguageProvider(cacheManager)
    }
}