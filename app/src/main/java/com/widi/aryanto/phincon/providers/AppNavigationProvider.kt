package com.widi.aryanto.phincon.providers

import androidx.navigation.NavController
import com.ramcosta.composedestinations.navigation.navigateTo
import com.widi.aryanto.core.common.provider.NavigationProvider
import com.widi.aryanto.features.settings.destinations.AboutScreenDestination
import com.widi.aryanto.features.settings.destinations.LanguageScreenDestination
import com.widi.aryanto.features.pokemon.detail.destinations.PokemonDetailScreenDestination

class AppNavigationProvider constructor(
    private val navController: NavController
) : NavigationProvider {

    override fun openPokemonDetail(name: String, url: String) {
        navController.navigateTo(PokemonDetailScreenDestination(name, url))
    }

    override fun openAppLanguage() {
        navController.navigateTo(LanguageScreenDestination)
    }

    override fun openAbout() {
        navController.navigateTo(AboutScreenDestination)
    }

    override fun navigateUp() {
        navController.navigateUp()
    }
}