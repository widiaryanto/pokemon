package com.widi.aryanto.phincon.app

import com.widi.aryanto.core.components.base.app.NetworkConfig
import com.widi.aryanto.phincon.BuildConfig

class NetworkConfigs : NetworkConfig() {
    override fun baseUrl(): String {
        return BuildConfig.BASE_URL
    }

    override fun timeOut(): Long {
        return 30L
    }
}