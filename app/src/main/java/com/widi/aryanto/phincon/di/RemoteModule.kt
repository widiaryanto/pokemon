package com.widi.aryanto.phincon.di

import android.content.Context
import com.widi.aryanto.core.components.base.app.NetworkConfig
import com.widi.aryanto.core.components.network.createHttpLoggingInterceptor
import com.widi.aryanto.core.components.network.createHttpRequestInterceptor
import com.widi.aryanto.core.components.network.createMoshi
import com.widi.aryanto.core.components.network.createOkHttpClient
import com.widi.aryanto.core.components.network.createRetrofitWithMoshi
import com.widi.aryanto.core.components.network.interceptor.HttpRequestInterceptor
import com.widi.aryanto.features.pokemon.data.remote.service.PokemonService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

private const val BASE_URL = "base_url"

@Module
@InstallIn(SingletonComponent::class)
class RemoteModule {

    @Provides
    @Singleton
    @Named(value = BASE_URL)
    fun provideBaseUrl(networkConfig: NetworkConfig): String {
        return networkConfig.baseUrl()
    }

    @Provides
    @Singleton
    fun provideMoshi(): com.squareup.moshi.Moshi {
        return createMoshi()
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): okhttp3.logging.HttpLoggingInterceptor {
        return createHttpLoggingInterceptor()
    }

    @Provides
    @Singleton
    fun provideHttpRequestInterceptor(): HttpRequestInterceptor {
        return createHttpRequestInterceptor()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        @ApplicationContext context: Context,
        httpLoggingInterceptor: okhttp3.logging.HttpLoggingInterceptor,
        httpRequestInterceptor: HttpRequestInterceptor
    ): okhttp3.OkHttpClient {
        return createOkHttpClient(
            isCache = true,
            interceptors = arrayOf(
                httpLoggingInterceptor,
                httpRequestInterceptor
            ),
            context = context
        )
    }

    @Provides
    @Singleton
    fun providePokemonService(
        @Named(value = BASE_URL) baseUrl: String,
        okHttpClient: okhttp3.OkHttpClient,
        moshi: com.squareup.moshi.Moshi
    ) = createRetrofitWithMoshi<PokemonService>(
        okHttpClient = okHttpClient,
        moshi = moshi,
        baseUrl = baseUrl
    )
}