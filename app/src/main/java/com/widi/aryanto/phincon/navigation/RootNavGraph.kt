package com.widi.aryanto.phincon.navigation

import com.ramcosta.composedestinations.spec.DestinationSpec
import com.ramcosta.composedestinations.spec.NavGraphSpec
import com.widi.aryanto.features.home.HomeNavGraph
import com.widi.aryanto.features.settings.SettingsNavGraph
import com.widi.aryanto.features.pokemon.detail.PokemonNavGraph

object RootNavGraph : NavGraphSpec {
    override val route = "root"

    override val destinationsByRoute = emptyMap<String, DestinationSpec<*>>()

    override val startRoute = HomeNavGraph

    override val nestedNavGraphs = listOf(
        HomeNavGraph,
        SettingsNavGraph,
        PokemonNavGraph
    )
}