package com.widi.aryanto.phincon.providers

import android.content.Context
import com.widi.aryanto.core.common.provider.ResourceProvider

class AppResourceProvider(private val context: Context) : ResourceProvider {
    override fun getString(id: Int): String {
        return context.getString(id)
    }
}