package com.widi.aryanto.phincon.app

import com.widi.aryanto.core.components.base.app.AppInitializer
import com.widi.aryanto.core.components.base.app.CoreApplication
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class PhinConApp : CoreApplication() {

    @Inject
    lateinit var initializer: AppInitializer

    override fun onCreate() {
        super.onCreate()
        initializer.init(this)
    }
}