package com.widi.aryanto.phincon.di

import android.content.Context
import com.widi.aryanto.features.pokemon.data.local.dao.PokemonFavoriteDao
import com.widi.aryanto.features.pokemon.data.remote.service.PokemonService
import com.widi.aryanto.features.pokemon.data.repository.PokemonRepository
import com.widi.aryanto.features.settings.data.repository.LanguageRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {
    @Singleton
    @Provides
    fun providePokemonRepository(
        service: PokemonService,
        dao: PokemonFavoriteDao
    ) = PokemonRepository(service, dao)

    @Singleton
    @Provides
    fun provideLanguageRepository(
        @ApplicationContext context: Context
    ) = LanguageRepository(context)
}