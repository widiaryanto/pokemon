package com.widi.aryanto.phincon.di

import com.widi.aryanto.features.pokemon.di.PokemonModule
import com.widi.aryanto.features.settings.di.LanguageModule
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(
    includes = [
        PokemonModule::class,
        LanguageModule::class
    ]
)
@InstallIn(SingletonComponent::class)
class DomainModule